import React from 'react';

import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'
import {
    faHome,
    faThLarge,
    faSearch,
    faShoppingCart,
    faUser,
    faChevronCircleLeft
} from '@fortawesome/free-solid-svg-icons'
import theme from '../styles/theme.style';
import Header from '../components/Header';

import HomeScreen from '../screens/HomeScreen';
import CategoriesListScreen from '../screens/CategoriesListScreen';
import ProductsListScreen from '../screens/ProductsListScreen';
import ProductDetailsScreen from '../screens/ProductDetailsScreen';
import SearchScreen from '../screens/SearchScreen';
import CartScreen from '../screens/CartScreen';
import UserScreen from '../screens/UserScreen';
import LoginScreen from '../screens/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen';
import AuthLoadingScreen from '../screens/AuthLoadingScreen';

const stackConfig = {
    defaultNavigationOptions: {
        headerStyle: {
            borderBottomWidth: 0,
        },
        headerBackTitle: null,
        headerBackImage: ({tintColor}) => <FontAwesomeIcon style={{margin: 12}} icon={faChevronCircleLeft}
                                                           color={tintColor} size={28}/>,
        headerTintColor: theme.SECONDARY_COLOR,
    }
};

// Home Stack Navigator
const HomeNavigator = createStackNavigator({
    Home: HomeScreen,
    ProductDetails: ProductDetailsScreen,
}, stackConfig);

// Products Stack Navigator
const ProductsNavigator = createStackNavigator({
        CategoriesList: CategoriesListScreen,
        ProductsList: ProductsListScreen,
        ProductDetails: ProductDetailsScreen,
    }, stackConfig
);

// Search Stack Navigator
const SearchNavigator = createStackNavigator({
    Search: {
        screen: SearchScreen,
        navigationOptions: {
            header: null
        }
    },
    ProductDetails: ProductDetailsScreen,
}, stackConfig);

// Cart Stack Navigator
const CartNavigator = createStackNavigator({
    Cart: CartScreen,
}, stackConfig);

// User Stack Navigator
const UserNavigator = createStackNavigator({
    User: {
        screen: UserScreen,
    },
}, stackConfig);

// Auth Stack Navigator
const AuthNavigator = createStackNavigator({
    Login: LoginScreen,
    Register: RegisterScreen,
}, {
    defaultNavigationOptions: {
        header: null,
    }
});

// Main Tab Navigator
const AppNavigator = createMaterialTopTabNavigator({
        Home: {
            screen: HomeNavigator,
            navigationOptions: {
                tabBarIcon: ({tintColor}) => <FontAwesomeIcon icon={faHome} color={tintColor}/>,
            }
        },
        CategoriesList: {
            screen: ProductsNavigator,
            navigationOptions: {
                tabBarIcon: ({tintColor}) => <FontAwesomeIcon icon={faThLarge} color={tintColor}/>,
            }
        },
        Search: {
            screen: SearchNavigator,
            navigationOptions: {
                tabBarIcon: ({tintColor}) => <FontAwesomeIcon icon={faSearch} color={tintColor}/>,
            }
        },
        Cart: {
            screen: CartNavigator,
            navigationOptions: {
                tabBarIcon: ({tintColor}) => <FontAwesomeIcon icon={faShoppingCart} color={tintColor}/>,
            }
        },
        User: {
            screen: UserNavigator,
            // screen: AuthNavigator,
            navigationOptions: {
                tabBarIcon: ({tintColor}) => <FontAwesomeIcon icon={faUser} color={tintColor}/>,
            }
        },
    },
    {
        tabBarOptions: {
            showIcon: true,
            showLabel: false,
            activeTintColor: theme.PRIMARY_COLOR,
            inactiveTintColor: theme.SECONDARY_COLOR,
            style: {backgroundColor: 'white'},
            indicatorStyle: {backgroundColor: theme.PRIMARY_COLOR},
        },
        swipeEnabled: false,
        tabBarPosition: 'bottom',
    }
);

export default createAppContainer(AppNavigator
    // createSwitchNavigator({
    //     AuthLoading: AuthLoadingScreen,
    //     App: AppNavigator,
    //     Auth: AuthNavigator,
    // }, { initialRouteName: 'AuthLoading'})
);
