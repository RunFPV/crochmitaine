import React from 'react';
import {Text, View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';

import theme from '../styles/theme.style'

class CategoryItem extends React.Component {

    _displayImage = () => {
        if (this.props.category.image) {
            return <Image style={styles.image} source={{uri: this.props.category.image}}/>
        }
    };

    _displayProductsCount = () => {
        const productCount = this.props.category.products_count;
        if (productCount > 1) {
            return <Text style={styles.text}>{productCount} produits</Text>
        }
        return <Text style={styles.text}>{productCount} produit</Text>
    };

    render() {
        return (
            <TouchableOpacity style={styles.container} onPress={() => this.props.displayProductsForCategory(this.props.category.id)}>
                {this._displayImage()}
                <View style={styles.textContainer}>
                    <Text style={[styles.text, styles.title]}>{this.props.category.title}</Text>
                    {this._displayProductsCount()}
                </View>
            </TouchableOpacity>
        )
    }
}

CategoryItem.propTypes = {
    category: PropTypes.object.isRequired,
    displayProductsForCategory: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
    container: {
        marginHorizontal: theme.DEFAULT_PADDING,
        marginVertical: 10,
        height: 175,
        justifyContent: 'center',
        backgroundColor: theme.SECONDARY_COLOR,
        borderRadius: 10,
    },
    image: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover',
        borderRadius: 10,
    },
    textContainer: {
        position: 'absolute',
        alignSelf: 'center',
    },
    title: {
        fontSize: theme.FONT_SIZE_LARGE,
        fontWeight: 'bold',
    },
    text: {
        textAlign: 'center',
        color: 'white',
    }
});

export default CategoryItem
