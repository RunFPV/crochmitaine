import React from 'react';
import {View, StyleSheet, ActivityIndicator} from 'react-native';

class Loading extends React.Component {
    render() {
        return (
            <View style={styles.mainContainer}>
                <ActivityIndicator/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%'
    }
});

export default Loading
