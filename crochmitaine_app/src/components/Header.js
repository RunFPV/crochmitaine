import React from 'react';
import {View, Text, StyleSheet, SafeAreaView} from 'react-native';
import {Header as rnHeader} from 'react-navigation-stack';


class Header extends React.Component {
    render() {
        return (
            <SafeAreaView style={styles.mainContainer}>
                <Text>Custom Header</Text>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        height: rnHeader.HEIGHT,
        flex: 1,
        backgroundColor: 'red',
    }
});

export default Header;
