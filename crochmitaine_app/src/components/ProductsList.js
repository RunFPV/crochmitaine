import React from 'react';
import {FlatList} from 'react-native';

import PropTypes from 'prop-types';

import ProductItem from './ProductItem';
import ListEmptyComponent from './ListEmptyComponent';

class ProductsList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: this.props.products
        }
    }


    _displayProductDetailsForProduct = (idProduct) => {
        this.props.navigation.navigate('ProductDetails', {idProduct: idProduct});
    };

    _onReachedEnd = () => {
        if (this.props.page < this.props.totalPages) {
            this.props.loadProducts();
        }
    };


    render() {
        return (
            <FlatList
                ListHeaderComponent={this.props.ListHeaderComponent}
                key={this.props.numColumns}
                data={this.props.products}
                renderItem={({item}) => <ProductItem
                    product={item}
                    numColumns={this.props.numColumns}
                    displayProductDetailsForProduct={this._displayProductDetailsForProduct}
                />}
                onEndReachedThreshold={0.1}
                onEndReached={this._onReachedEnd}
                keyExtractor={item => item.id.toString()}
                contentContainerStyle={{flexGrow: 1}}
                ListEmptyComponent={ListEmptyComponent}
                numColumns={this.props.numColumns}
            />
        );
    }
}

ProductsList.propTypes = {
    navigation: PropTypes.object.isRequired,
    products: PropTypes.array.isRequired,
    loadProducts: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    totalPages: PropTypes.number.isRequired,
    numColumns: PropTypes.number.isRequired,
    ListHeaderComponent: PropTypes.object,
};

export default ProductsList;
