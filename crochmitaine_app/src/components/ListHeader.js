import React from 'react';
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';
import PropTypes from 'prop-types';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faThLarge, faStop, faSort} from '@fortawesome/free-solid-svg-icons';
import theme from '../styles/theme.style';

class ListHeader extends React.Component {
    render() {
        return (
            <View style={styles.mainContainer}>
                <View style={styles.iconContainer}>
                    <TouchableOpacity style={styles.btn} onPress={this.props.showActionSheet}>
                        <FontAwesomeIcon icon={faSort} size={24}/><Text> Les filtres</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.iconContainer}>
                    <TouchableOpacity onPress={() => this.props.changeNumColumns(1)}>
                        <FontAwesomeIcon style={styles.icon} icon={faStop} size={24}
                                         color={this.props.numColumns === 1 ? 'black' : theme.SECONDARY_COLOR}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.changeNumColumns(2)}>
                        <FontAwesomeIcon style={styles.icon} icon={faThLarge} size={24}
                                         color={this.props.numColumns === 2 ? 'black' : theme.SECONDARY_COLOR}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

ListHeader.propTypes = {
    changeNumColumns: PropTypes.func.isRequired,
    showActionSheet : PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
    mainContainer: {
        flexDirection: 'row',
        paddingHorizontal: theme.DEFAULT_PADDING,
        paddingVertical: 5,
        justifyContent: 'space-between'
    },
    iconContainer: {
        flexDirection: 'row',
    },
    icon: {
        marginHorizontal: 5,
    },
    btn: {
        flexDirection: 'row',
        alignItems: 'center',
    }
});

export default ListHeader
