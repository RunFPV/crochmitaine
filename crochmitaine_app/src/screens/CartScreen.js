import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'
import {faShoppingCart} from '@fortawesome/free-solid-svg-icons'

import theme from '../styles/theme.style';
import {Button} from "react-native-elements";

class CartScreen extends React.Component {
    render() {
        return (
            <View style={styles.mainContainer}>
                <View style={styles.subContainer}>
                    <FontAwesomeIcon icon={faShoppingCart} size={64} color={theme.SECONDARY_COLOR}/>
                    <Text style={styles.title}>Votre panier est vide</Text>
                    <Text styles={styles.subTitle}>Ajoutez un produit à votre panier</Text>
                    <Button title="Acheter maintenant !"
                            onPress={() => this.props.navigation.navigate('CategoriesList')}
                            containerStyle={{paddingVertical: 15}}
                            buttonStyle={{backgroundColor: theme.PRIMARY_COLOR}}/>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        padding: theme.DEFAULT_PADDING,
        alignItems: 'center',
    },
    subContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontWeight: 'bold',
        fontSize: theme.FONT_SIZE_LARGE,
        paddingVertical: 15,
        textAlign: 'center',
    },
    subTitle: {
        textAlign: 'center',
    }
});

export default CartScreen;
