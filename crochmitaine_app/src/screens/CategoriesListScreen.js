import React from 'react';
import {View, StyleSheet, FlatList} from 'react-native';

import {getCategories} from '../services/api';

import CategoryItem from '../components/CategoryItem';
import ListEmptyComponent from '../components/ListEmptyComponent';


class CategoriesListScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            categories: [],
            loading: true,
        };

        getCategories()
            .then((resp) => {
                this.setState({categories: resp.data.results});
            })
            .catch((error) => {
                console.log(error.response);
            })
            .finally(() => this.setState({loading: false}));
    }

    _displayProductsForCategory = (idCategory) => {
        this.props.navigation.navigate('ProductsList', {idCategory: idCategory});
    };


    render() {
        return (
            <View style={styles.mainContainer}>
                <FlatList
                    data={this.state.categories}
                    renderItem={({item}) => <CategoryItem
                        category={item}
                        displayProductsForCategory={this._displayProductsForCategory}
                    />}
                    keyExtractor={item => item.id.toString()}
                    contentContainerStyle={{flexGrow: 1}}
                    ListEmptyComponent={ListEmptyComponent}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
});

export default CategoriesListScreen;
