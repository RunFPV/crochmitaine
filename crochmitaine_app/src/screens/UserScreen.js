import React from 'react';
import {ScrollView, View, StyleSheet, Text, Switch} from 'react-native';
import {ListItem, Avatar} from 'react-native-elements';

import theme from '../styles/theme.style';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faHeart, faGlobeEurope, faBell, faScroll, faUserLock} from '@fortawesome/free-solid-svg-icons';

class UserScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            notifSwitch: false,
            darkModeSwitch: true,
        }
    }


    _toggleValue = () => {
        alert("coucou");
        // alert(value);
        // this.setState({notifSwitch: !this.state.notifSwitch})
    };

    render() {

        let list = [
            {
                name: 'Liste des favoris (0)',
                icon: <FontAwesomeIcon icon={faHeart}/>,
                nav: 'fav',
                chevron: true
            },
            {
                name: 'Langues',
                icon: <FontAwesomeIcon icon={faGlobeEurope}/>,
                nav: 'languages',
                chevron: true
            },
            {
                name: 'Notifications',
                icon: <FontAwesomeIcon icon={faBell}/>,
                switch: <Switch value={this.state.notifSwitch} onValueChange={this._toggleValue}/>
            },
            {
                name: 'Mode nuit',
                icon: <FontAwesomeIcon icon={faHeart}/>,
                switch: <Switch value={this.state.darkModeSwitch} onValueChange={() => alert("Mode Change")}/>
            },
            {
                name: 'Politique de confidentialité',
                icon: <FontAwesomeIcon icon={faUserLock}/>,
                nav: 'PolitikConf',
                chevron: true
            },
            {
                name: 'Terme et condition',
                icon: <FontAwesomeIcon icon={faScroll}/>,
                nav: 'TermAndCondition',
                chevron: true
            },
        ];

        return (
            <ScrollView style={styles.mainContainer}>
                <View style={styles.profile}>
                    <Avatar size={'xlarge'} overlayContainerStyle={{backgroundColor: theme.PRIMARY_COLOR}} rounded
                            onPress={() => alert('tu veux voir tn profile toi')}
                            showEditButton editButton={{name: 'user', type: 'font-awesome'}}
                            source={{uri: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',}}/>
                    <Text style={styles.title}>NOM PRENOM</Text>
                </View>
                {list.map((item, key) => (
                    <ListItem
                        key={key}
                        title={item.name}
                        bottomDivider
                        leftIcon={item.icon}
                        chevron={item.chevron}
                        switch={item.switch}
                        onPress={item.nav ? () => this.props.navigation.navigate(item.nav) : null}
                    />
                ))}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        paddingHorizontal: theme.DEFAULT_PADDING,
    },
    profile: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        paddingVertical: 10,
        fontSize: theme.FONT_SIZE_LARGE,
    }
});

export default UserScreen;
