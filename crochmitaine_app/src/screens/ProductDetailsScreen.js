import React from 'react';
import {View, Text, Image, StyleSheet, TouchableHighlight} from 'react-native';

import {getProductDetails} from '../services/api';

import theme from '../styles/theme.style';
import AwesomeButton from 'react-native-really-awesome-button';

import ProductImageModal from '../components/ProductImageModal';
import Loading from '../components/Loading'


class ProductDetailsScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            product: [],
            loading: true,
            modalVisible: false,
        };

        getProductDetails(this.props.navigation.getParam('idProduct'))
            .then((resp) => {
                this.setState({product: resp.data});
            })
            .catch((error) => {
                console.log(error.response);
            })
            .finally(() => this.setState({loading: false}));
    }

    _displayAddButton = () => {
        if (!this.state.product.quantity) {
            return <AwesomeButton style={styles.button} $
                                  disabled
                                  backgroundColor={'red'}
                                  backgroundDarker={theme.SECONDARY_COLOR}
                                  raiseLevel={1}
            >Rupture de stock</AwesomeButton>
        }
        return <AwesomeButton style={styles.button}
                              backgroundColor={theme.PRIMARY_COLOR}
                              backgroundDarker={theme.SECONDARY_COLOR}
                              raiseLevel={1}
                              onPress={() => alert("J'ajoute au panier !")}
        >Ajouter</AwesomeButton>
    };


    _toggleModal = () => {
        this.setState({modalVisible: !this.state.modalVisible})
    };

    render() {
        if (this.state.loading) {
            return <Loading/>
        }
        return (
            <View style={styles.mainContainer}>
                <ProductImageModal visible={this.state.modalVisible} toggleModal={this._toggleModal} images={this.state.product.more_images}/>
                <View style={styles.imageContainer}>
                    <TouchableHighlight onPress={this._toggleModal} disabled={this.state.product.more_images.length === 0}>
                        <Image style={styles.image} source={{uri: this.state.product.image}}/>
                    </TouchableHighlight>
                </View>
                <Text style={styles.title}>{this.state.product.title}</Text>
                <Text style={styles.price}>{this.state.product.price_ttc} €</Text>

                <View style={styles.subContainer}>
                    <Text style={styles.description}>{this.state.product.description}</Text>
                    <Text style={styles.description}>{this.state.product.description2}</Text>
                </View>
                {this._displayAddButton()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        padding: theme.DEFAULT_PADDING,
    },
    imageContainer: {
        flex: 1,
    },
    image: {
        width: '100%',
        height: '100%',
        backgroundColor: theme.SECONDARY_COLOR,
    },
    subContainer: {
        justifyContent: 'center',
        // alignItems: 'center',
    },
    title: {
        fontWeight: 'bold',
        fontSize: theme.FONT_SIZE_LARGE,
        paddingVertical: 15,
        textAlign: 'center',
    },
    description: {
        paddingVertical: 5,
    },
    price: {
        textAlign: 'center',
        color: theme.SECONDARY_COLOR,
    },
    button: {
        alignSelf: 'center',
    }
});

export default ProductDetailsScreen;
