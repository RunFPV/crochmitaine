import React from 'react';
import {StyleSheet, SafeAreaView} from 'react-native';
import {Input} from 'react-native-elements';

import {getProducts} from '../services/api';
import ProductsList from '../components/ProductsList';

import Icon from 'react-native-vector-icons/FontAwesome';
import theme from '../styles/theme.style';


class SearchScreen extends React.Component {
    constructor(props) {
        super(props);

        this.totalPages = 0;
        this.page = 0;
        this.searchedText = '';

        this.state = {
            products: [],
            loading: true,
            numColumns: 2,
        };
    }

    _loadProducts = () => {
        if (this.searchedText !== '') {
            this.setState({loading: true});
            getProducts({...this.state.params, page: this.page + 1, search: this.searchedText})
                .then((resp) => {
                    this.totalPages = resp.data.total_pages;
                    this.page = resp.data.page;
                    this.setState({
                        products: [...this.state.products, ...resp.data.results]
                    })
                })
                .catch((error) => {
                    console.log(error.response);
                })
                .finally(() => this.setState({loading: false}));
        }
    };

    _searchTextInputChanged = (text) => {
        this.searchedText = text;
    };

    _searchProducts = () => {
        this.page = 0;
        this.totalPages = 0;
        this.setState({products: []}, () => {
            this._loadProducts();
        });
    };


    render() {
        return (
            <SafeAreaView style={styles.mainContainer}>
                <Input
                    inputStyle={{color: theme.SECONDARY_COLOR}}
                    leftIcon={<Icon size={15}f name='search' color={theme.SECONDARY_COLOR} style={{paddingRight: 10}} />}
                    autoCorrect={false}
                    placeholder="Rechercher"
                    onChangeText={(text) => this._searchTextInputChanged(text)}
                    onSubmitEditing={() => this._searchProducts()}
                    returnKeyType='search'
                />

                <ProductsList navigation={this.props.navigation}
                              products={this.state.products}
                              numColumns={this.state.numColumns}
                              loadProducts={this._loadProducts}
                              page={this.page}
                              totalPages={this.totalPages}/>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        paddingHorizontal: theme.DEFAULT_PADDING,
        justifyContent: 'center',
    },
});

export default SearchScreen;
