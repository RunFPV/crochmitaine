import React  from 'react';
import {ActivityIndicator, StyleSheet, Text, View, AsyncStorage} from 'react-native';

class AuthLoadingScreen extends React.Component {

    // Fetch the token from storage then navigate to our appropriate place
    _bootstrapAsync = async () => {
        const userToken = await AsyncStorage.getItem('userToken');

        // This will switch to the App screen or Auth screen and this loading
        // screen will be unmounted and thrown away.
        this.props.navigation.navigate(userToken ? 'App' : 'Auth');
    };

    componentDidMount() {
        this._bootstrapAsync();
    }

    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator/>
                <Text>Loading</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
});


export default AuthLoadingScreen;
